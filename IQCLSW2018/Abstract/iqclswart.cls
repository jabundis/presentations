%% LaTeX IQCLSW Confernce Article Template
%% Author: Michael Riesch (michael.riesch@tum.de)
%% Computational Photonics Group, TUM
%% March 2018

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{iqclswart}[2018/03/13 Article for IQCLSW Conference]

% load article base class
\LoadClass[11pt,a4paper,bibtotoc,notitlepage]{article}

% set paper size and margins
\RequirePackage[a4paper,margin=3cm]{geometry}

% avoid widows and orphans
\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000

% load base packages
\RequirePackage[english]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{placeins}
\RequirePackage{graphicx}
\RequirePackage[hidelinks,breaklinks]{hyperref}

% set Times New Roman as default font
\RequirePackage{mathptmx}
\renewcommand{\familydefault}{\rmdefault}

% define 16pt font
\makeatletter
\renewcommand\LARGE{\@setfontsize\LARGE{16}{19}}
\makeatother

% no page numbers
\pagestyle{empty}

% custom title
\makeatletter
\renewcommand{\maketitle}{%
  \newpage
  \begin{center}
    \LARGE \textbf{\@title}\par

    \vspace{16pt}

    \large \@author \par
    \vspace{4pt}
  \end{center}
  \thispagestyle{empty}
  \FloatBarrier
}
\makeatother

% custom figure captions
\RequirePackage{caption}
\DeclareCaptionLabelFormat{figureabbr}{Fig.~#2}
\captionsetup[figure]{labelformat=figureabbr,labelfont=bf,font=footnotesize}

% custom author + affiliation blocks
\RequirePackage{authblk}
\setlength{\affilsep}{0pt}
\renewcommand\Authfont{\bfseries}
\renewcommand\Affilfont{\mdseries\itshape\small}

% custom section titles
\RequirePackage{titlesec}
\RequirePackage{tabto}
\titleformat{\section}{\normalfont\large\bfseries}{\thesection.}{2em}{}
\titlespacing{\section}{0pt}{14pt plus 0pt minus 0pt}{6pt plus 0pt minus 0pt}

% adjust parameter spacing
\setlength{\parskip}{4pt}
\setlength{\parindent}{0pt}
