\documentclass{tum-poster}

\usepackage[style=nature, backend=bibtex]{biblatex}
\addbibresource{literature.bib}
\graphicspath{{Figures/}}

\title{Colliding Pulse Mode Locking of Quantum Cascade Lasers}
\author{Jesus Abundis-Patino, Michael Riesch, Petar Tzenov, Christian Jirauschek}
\institute{Department of Electrical and Computer Engineering, Technical University of Munich, 80333 Munich, Germany}

\usepackage{tcolorbox}
\setlength{\parskip}{5mm}


\begin{document}

\maketitle

\begin{multicols}{2}
\begin{tcolorbox}[colback=white, colframe=TUMBlau]


\section{Introduction}
%\setlength{\parskip}{5mm}

\begin{itemize}
\item A quantum cascade laser (QCL) is a relatively new semiconductor laser whose gain medium consists of alternating nanoscale heterostructures creating multiple quantum wells (see Fig.~\ref{fig:QCL}).
\item Does not depend on electron-hole recombination but instead is a unipolar device where the electron moves across the energy eigenstates created along the quantum wells.
\item By engineering the gain medium,  intense, coherent light from the mid-infrared through the terahertz regime can be obtained.
\end{itemize}


\begin{figure}
\begin{center}
\captionsetup{type=figure}
\addtocounter{figure}{-1}
%\includegraphics[width=0.8\textwidth]{finalFigures/QCLAndgainMedium}
\includegraphics[height=13cm]{Figures/QCLAndgainMedium}
    \caption{(a) Depiction of a quantum cascade laser. (b) Conduction band profile of the QCL gain medium along with the energy states.}
\label{fig:QCL}
\end{center}
\end{figure}


Despite being the most potent and compact direct sources of THz light QCLs have proven difficult to mode lock due to the complex interplay among:


\begin{itemize}
\item Fast carrier relaxation times,
\item Giant nonlinearities,
\item Spatial hole burning.
\end{itemize}


Recent studies indicate that the light emitted from free running QCLs, is both frequency and amplitude modulated, with the latter modulation occurring at very short time-scales~\cite{benea2017intensity}.

In this work we show that one can exploit this natural “multi-pulse” regime of operation to achieve
a very stable second harmonic generation via the colliding pulse mode locking (CPML)
technique~\cite{chen1992Monolithic}. 


\end{tcolorbox}


\vspace{5mm}  
\begin{tcolorbox}[colback=white, colframe=TUMBlau]


\section{Design}
%\setlength{\parskip}{5mm}

The cavity geometry \cite{tzenov2018passive} is illustrated in Fig.~\ref{fig:geometry}. 

\begin{itemize}
\item Gain media of equal length, constructed from slow gain recovery, bound-to-continuum QCLs.
\item A fast saturable absorber contains the same quantum well heterostructure as the gain.
\item Absorber is isolated and biased such that it acts as a detector.
\item When fast recovery of the absorber inversion and ultra-strong coupling to the optical field is met, generation of colliding pulses becomes posiible.
\end{itemize}


\vspace{20pt}
\begin{figure}
\begin{center}
\captionsetup{type=figure}
\addtocounter{figure}{-1}
\includegraphics[width=0.8\textwidth]{Figures/CPMLgeometry}
\caption{An example of multi-section Fabry-Perot (FP) cavity geometry, consisting of spatially separated gain and
absorber regions for colliding pulse mode locking.}
\label{fig:geometry}
\end{center}
\end{figure}


\end{tcolorbox}


\vspace{5mm}
\begin{tcolorbox}[colback=white, colframe=TUMBlau]


\section{Model}
%\setlength{\parskip}{5mm}

We employ semi-classical models for both the gain and the absorber,
based on the solution of the full Maxwell-Bloch equations~\cite{riesch2018performance}. 

Advantages over classical-rate-equations-based models and its characteristics:

\begin{itemize}
\item Resolves the electromagnetic field in space and time.
\item Light-matter interaction quantum-mechanically determined via von Neumann equations.
\item Includes: dispersion, optical nonlinearities, and spatial hole burning.
\end{itemize}

Notably, we do not employ the rotating wave approximation which fails for large
spectral pulse widths. Furthermore, with the goal of reproducibility 
we have released our simulation framework as open source ~\cite{riesch2018performance,riesch2018mbsolve}.


\end{tcolorbox}


\columnbreak
\vfill
\begin{tcolorbox}[colback=white, colframe=TUMBlau]


\section{Results}
%\setlength{\parskip}{5mm}


\begin{itemize}
\item Calculations based on parameters in~\cite{tzenov2018passive} indicate that CPML is possible for gain recovery times (\textasciitilde 10 ps) as fast as 3 times shorter than the cavity round trip time.
\item From Fig.~\ref{fig:results} (a), we see that after 100 round trips of the light, the field splits into two identical pulses, each with a width of approximately 850 fs.
\item Beatnote and optical spectrum results in Fig.~\ref{fig:results} (b) and (c),  confirm the second harmonic character of the mode locking.
\item The calculated phases indicate that a fixed phase relationship is established between lasing Fabry-Perot modes.
\end{itemize}


\begin{figure}
\begin{center}
\captionsetup{type=figure}
\addtocounter{figure}{-1}
\def\svgwidth{1.0\textwidth}
\input{Figures/simResults.pdf_tex}
\caption{(a) Time domain profile of the electric field inside the cavity, over time duration of two round trip times ($T_\mathrm{rt}$). (b) Electric field intensity versus time of a single pulse. (c) A log plot of the beatnote signal versus round trip frequency~$f_\mathrm{rt}$. (d) Optical spectrum and modal phases of the field emitted from the right facet of the cavity.}
\label{fig:results}
\end{center}
\end{figure}


\end{tcolorbox}


%\vspace{5mm}
\vfill
\begin{tcolorbox}[colback=white, colframe=TUMBlau]


\section{Conclusions}
%\setlength{\parskip}{5mm}


\begin{itemize}
\item CPML is proposed for the generation of few cycle terahertz pulses from QCLs.
\item Robustness of the approach is shown by means of extended Maxwell-Bloch equations.
\item Dependence of the second harmonic mode locking onto the cavity length, asymmetries in the geometry, and variation of the injection current is investigated.
\item Results indicate that CPML is a promising alternative to active mode locking or conventional passive mode locking as it relaxes the stringent requirement onto the gain recovery versus cavity round trip times.
\end{itemize}


\end{tcolorbox}


%\vspace{5mm}
\vfill
\begin{tcolorbox}[colback=white, colframe=TUMBlau]


\section*{Acknowledgments}
%\setlength{\parskip}{5mm}

    This work was supported by the \textit{Universidad Aut\'onoma de Sinaloa}, through the grant program \textit{Doctores Jovenes}, and by the German Research Foundation (DFG JI 115/4-2, JI 115/9-1).

\end{tcolorbox}


%\vspace{5mm}
\vfill
\begin{tcolorbox}[colback=white, colframe=TUMBlau]


%\setlength{\parskip}{5mm}

\printbibliography


\end{tcolorbox}
\end{multicols}

\clearpage

\end{document}

